# UserModule

# Project Setup

* `composer update`

# Database Setup

* `php artisan make migrate`

# Create JWT key
* `php artisan jwt:secret`

# Run 
* `php artisan serve` 