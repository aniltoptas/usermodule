<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'telephone' => 'required|string|max:255',
            'password' => 'required|string|min:6|max:50',
            'email' => 'required|email|unique:users,email',
            'photo' => 'required|image|mimes:jpeg,png,jpg|max:10048'
        ]);

        $validatedData['photo_path'] = $this->uploadPhoto($request->file('photo'));
        $validatedData['password'] = Hash::make($request->password);

        unset($validatedData['photo']);

        $user = User::create($validatedData);

        return response()->json(['code' => 0, 'message' => $user]);

    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'password' => 'required|string|min:6|max:50',
            'email' => 'required|email'
        ]);

        if (!$token = auth('api')->attempt($credentials)) {
            return response()->json(['code' => 3, 'message' => 'Geçersiz şifre veya mail']);
        }

        return response()->json([
            'code' => 0,
            'message' => [
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth('api')->factory()->getTTL() * 60
            ]
        ]);
    }


    public function edit(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'string|max:255',
            'surname' => 'string|max:255',
            'telephone' => 'string|max:255',
            'password' => 'string|min:6|max:50',
            'email' => 'email|unique:users,email',
            'photo' => 'image|mimes:jpeg,png,jpg|max:10048'
        ]);

        $user = Auth::user();

        $photo = $request->file('photo');

        if (!empty($photo)) {
            $this->removePhoto($user->photo_path);

            $validatedData['photo_path'] = $this->uploadPhoto($photo);

            unset($validatedData['photo']);
        }

        if (!empty($request->password)) {
            $validatedData['password'] = Hash::make($request->password);
        }

        $user->update($validatedData);

        return response()->json(['code' => 0, 'message' => $user]);
    }

    public function get(){
        return response()->json(['code' => 0, 'message' => Auth::user()]);
    }

    private function getPhotoFolder()
    {
        return public_path('user-image');
    }

    private function uploadPhoto($photo)
    {
        $extension = $photo->guessClientExtension();
        $fileName = Str::random(32) . '.' . $extension;

        $photo->move($this->getPhotoFolder(), $fileName);

        return $fileName;
    }

    private function removePhoto($photo_path)
    {
        unlink($this->getPhotoFolder() . '/' . $photo_path);
    }
}
