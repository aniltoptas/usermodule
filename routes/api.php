<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'user'], function(){
    Route::get('/', 'UserController@get', function (){})->middleware('jwt.auth');
    Route::post('register','UserController@register');
    Route::post('login','UserController@login');
    Route::post('edit','UserController@edit',function (){})->middleware('jwt.auth');
});
